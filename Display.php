<!-- Ajlaa Hamdan
6/6/2023 || 8:43 
TestIv -->

<?php 
$Voltage=$_GET['Voltage'];
$Current=$_GET['Current'];
$Current_rate=$_GET['Current_rate'];



?>


<!DOCTYPE html>
<html>
    <head>
        <title>Bill</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!--boostrap-->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <!--Font-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    </head>

    <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-dark">
    <div class="container-fluid">
        <a class="active" href="Test.php" style="color:white;"><i class="fa fa-fw fa-home" style="color:white;"></i> Home</a> 
        <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
            </div>
        </div>
    </div>
</nav>
 <div class="container" style="padding-top: 70px;">
 <table class="table table-hover text-center">
  <thead class="thead-dark">
<!-- to display -->
    <tr>
    <th scope="col">No.</th> 
      <th scope="col">Power</th>
      <th scope="col">Energy</th>
      <th scope="col">Hours</th>
      <th scope="col">Total(RM)</th>
    </tr>
  </thead>
    <?php
    for($i= 1; $i <= 3; $i++)
    {
        $Power = $Voltage*$Current;
        $Energy = ($Power*$i*1000)/1000;
        $TotalE = $Energy*($Current_rate/100);
        // untk display
        echo"<tr>";
        echo "<th>" . $i. "</th>";
        echo "<th>" . number_format((float)$Power, 2, '.', ''). "</th>";
        echo "<th>" . number_format((float)$Energy, 2, '.', ''). "</th>";
        echo "<th>" . $i. "</th>";
        echo "<th>" . number_format((float)$TotalE, 2, '.', ''). "</th>";
        echo"<tr>";
         

    }
    ?>
</table>

</div> 
    </body>
</html>
