
<!-- Ajlaa Hamdan
6/6/2023 || 8:43 
TestIv -->



<!DOCTYPE html>

<html>

<body>

<!DOCTYPE html>
<html>
    <head>
        <title> Bill Calculation </title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!--boostrap-->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <!--Font-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" /
    </head>
<!-- form isi input -->
    <body>
        <div class="container"  style="padding-top: 70px;">
            <div class="text-center mb-4">
                <h3> Bill</h3>
                <p class="text-muted">Complete the form below to calculate your bill</p>
            </div>

            <div class="container d-flex justify-content-center">
                <form action="Display.php" method="Get" style="width;50vw; min-width: 300px;">
                    <div class="row">
                        <div class="col">
                            <label class="form-label">Voltage (V)</label>
                            <input type="text" class="form-control" name="Voltage" placeholder=" Voltage" required>
                        </div>
                </div>
                <div>
                    <label class="form-label">Current (A)</label>
                    <input type="text" class="form-control" name="Current"  placeholder="Current" required>
                </div>
                  <div>
                    <label class="form-label">Current Rate (sen/kWh)</label>
                    <input type="text" class="form-control" name="Current_rate" placeholder="Current Rate" required>
                </div>
                <br>
                <br>
                <div class="gap-4 d-flex justify-content-center" > 
                    <button type="Submit" class="btn btn-success" name="Submit">Calculate</button>
                    &nbsp;
                    <a href="Test.php" class="btn btn-danger">Cancel</a>
                    <br>
                </div>
                </form>

            </div>
            
        </div>
        
    </body>
</html>
